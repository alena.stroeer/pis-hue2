# PIS Hausübung 2

## Client
Program that provides a graphical interface for using the services of the server and making
use of the services of the server.

### Start

To start the client, just run:

```bash
./gradlew :client:run
```

### Build

To build the client jar, just run:

```bash
./gradlew :client:jar
```

The jar package will be under `client/build/libs/client-1.0-SNAPSHOT.jar`

### Javadoc

To generate the Java documentation, just run:
```bash
./gradlew :client:javadoc
```
The documentation  will be under `client/build/docs/javadoc/`

### Classdiagram
![Client class diagram](Client.png)

## Server

The server is an application program which provides the access for the client.
The server provides functions for listing the files on the server, downloading and deleting server files.
Furthermore, it provides the service to upload files to the server. 

### Start



To start the server, just run:

```bash
./gradlew :server:run
```

### Build

To build the server jar, just run:

```bash
./gradlew :server:jar
```

The jar package will be under `server/build/libs/server-1.0-SNAPSHOT.jar`

### Javadoc

To generate the Java documentation, just run:
```bash
./gradlew :server:javadoc
```
The documentation  will be under `server/build/docs/javadoc/`

### Classdiagram
![Server class diagram](Server.png)

## Common
The common package is just a library, used from client and server.
### Javadoc

To generate the Java documentation, just run:
```bash
./gradlew :common:javadoc
```
The documentation  will be under `common/build/docs/javadoc/`