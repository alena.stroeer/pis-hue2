@startuml

package server {
class ServerLauncher {
    +{static}main(args: Array<String>)
}

class Server {
    -port: int
    -fileOperator: FileOperator
    -MAX_CONNECTION: int
    -serverSocket: ServerSocket
    -log: SyncList <LogEntry>
    -session: SyncList <Session>


    -constructor(port: Integer, fileOperator: FileOperator)
    +startServer()
    +stopServer()
    +acceptConnection(socket: Socket) throws IOException
    +removeSession(session: Session)
    +log(l: LogEntry)
    +getFileOperator():FileOperator
    -declineConnection(socket: Socket) throws IOException
}

class "Session: Runnable" as ServerSession {
 -server: Server
 -socket: Socket
 -reader: BufferedReader
 -writer:PrintWriter
 -instructionHandler: InstructionHandler
+constructor(server: Server, socket: Socket): Session throws IOException
+closeConnection()
+run()
-handleGET() throws IOException
-handlePUT() throws IOException
-handleDEL() throws IOException
-handleLST() throws IOException
-handleDSC()
}


}

package common {
    class FileOperator {
        -path: String
        +constructor(path: String): FileOperator
        +getFiles(): Array<String>
        +getFile(fileName: String): ByteArrayOutputStream
        +deleteFile(fileName: String): Boolean
        +writeFile(fileName: String, data: Array<Byte>): Boolean
    }

    class InstructionHandler{
        -reader: BufferedReader
        -writer:PrintWriter
        -remoteAddress: SocketAddress
        +constructor(reader: BufferedReader, writer: PrintWriter,remoteAddress: SocketAddress)
        +sendInstruction()
        +sendInstruction(sendInstruction: Instruction)
        +sendInstruction(sendInstruction: Instruction, promise: Instruction, errorMessage: String) throws IOException
        +sendInstruction(sendInstruction; Instruction, errorMessage: String, filename: String) throws IOException)
        +receivePromise(sendInstruction: Instruction, promise: Instruction, errorMessage: String, filename: String) throws IOException
    }

    enum Instruction {
        CON
        DSC
        PUT
        GET
        DEL
        LST
        ACK
        DND
        +send(writer: PrintWriter, data: ByteArrayOutputStream)
        +send(writer: PrintWriter, filenames: String...)
        +{static}receive(reader: BufferedReader) throws IOException
    }
}

package common.util {
    class "SyncList<T>" as SyncList {
        -list: List<T>
        +add(item T)
        +get(index Integer): T
        +size(): Integer
        +remove(item T): Boolean
    }

    class "LogEntry" as LogEntry{
        -send: Instruction
        -filename: String
        -successful: boolean
        -address: SocketAddress
        -timestamp: Timestamp
        -response: Instruction
        +constructor(instruction: Instruction, filename: String , address: SocketAddress, successful: boolean)
        +constructor(send: Instruction, fileName: String, address: SocketAddress, successful: boolean, response: Instruction)
        +toString(): String
    }

    class "Logger" as Logger{
        -{static}instance: Logger
        -logs: List<LogEntry>
        -propertyChangeSupport: PropertyChangeSupport
        -constructor()
        +{static}getInstance(): Logger
        +addLogEntry(logEntry: LogEntry)
        +getPropertyChangeSupport(): PropertyChangeSupport
    }
}
ServerLauncher -- Server
Server *-- ServerSession
LogEntry o-- InstructionHandler
LogEntry o-- Server
LogEntry o-- ServerSession
Logger o--InstructionHandler
SyncList o--Server
FileOperator o-- ServerLauncher
FileOperator o-- Server
Instruction o-- InstructionHandler
Instruction o-- LogEntry
Instruction o--Server
Instruction o--ServerSession
InstructionHandler o--ServerSession


@enduml
