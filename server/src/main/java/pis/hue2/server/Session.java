package pis.hue2.server;

import pis.hue2.common.Instruction;
import pis.hue2.common.InstructionHandler;
import pis.hue2.common.util.LogEntry;

import java.io.*;
import java.net.Socket;

/**
 * The class takes care of the clients instructions and processes them
 */
public class Session implements Runnable {

    /**
     * a variable from type Server, which allows the access to the methods from the class server
     */
    private final Server server;

    /**
     * Socket to establish a communication with client
     */
    private final Socket socket;

    /**
     * BufferedReader to read the incoming instructions from the client
     */
    private final BufferedReader reader;

    /**
     * PrintWriter to send instructions to the client
     */
    private final PrintWriter writer;

    /**
     * InstructionHandler to get access to methods of the InstructionHandler
     */
    private final InstructionHandler instructionHandler;

    /**
     * Construction of the class Session.
     * Creates the reader and the writer for the communication with the client.
     * Includes a welcome protocol.
     *
     * @param server the Server that holds this Session
     * @param socket the Socket that is used for Communication
     * @throws IOException if something went wrong with the socket
     */
    public Session(Server server, Socket socket) throws IOException {
        this.server = server;
        this.socket = socket;
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new PrintWriter(socket.getOutputStream());
        instructionHandler = new InstructionHandler(reader, writer, socket.getRemoteSocketAddress());

        instructionHandler.sendInstruction();
        server.log(new LogEntry(Instruction.CON, "", socket.getRemoteSocketAddress(), true));
    }

    /**
     * Method closes the connection to the client and clears the communication socket.
     * Furthermore it removes the session from the server.
     */
    public void closeConnection() {
        try {
            instructionHandler.sendInstruction(Instruction.DSC);
            socket.close();
            server.removeSession(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method listens on the connection and processes the protocol specifications.
     */
    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                if (socket.getInputStream().available() > 0) {
                    String in = reader.readLine();
                    if (in.length() <= 0) continue;
                    switch (Instruction.valueOf(in)) {
                        case GET -> handleGET();
                        case PUT -> handlePUT();
                        case DEL -> handleDEL();
                        case LST -> handleLST();
                        case DSC -> handleDSC();
                        default -> Instruction.DND.send(writer);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                instructionHandler.sendInstruction(Instruction.DND);
            }
        }
    }

    /**
     * Method to cope with downloading a file from the server.
     *
     * @throws IOException if the server gets an unexpected response from the server during the communication
     */
    private void handleGET() throws IOException {
        String filename = reader.readLine();
        instructionHandler.sendInstruction();

        String in = reader.readLine();
        if (!in.equals(Instruction.ACK.toString())) {
            server.log(new LogEntry(Instruction.GET, filename, socket.getRemoteSocketAddress(), false));
            instructionHandler.sendInstruction(Instruction.DND);
            return;
        }

        ByteArrayOutputStream byteArrayOutputStream = server.getFileOperator().getFile(filename);
        if (byteArrayOutputStream == null) {
            server.log(new LogEntry(Instruction.GET, filename, socket.getRemoteSocketAddress(), false));
            instructionHandler.sendInstruction(Instruction.DND);
            return;
        }
        Instruction.DAT.send(writer, byteArrayOutputStream);
        byteArrayOutputStream.close();

        in = reader.readLine();
        if (!in.equals(Instruction.ACK.toString())) {
            server.log(new LogEntry(Instruction.GET, filename, socket.getRemoteSocketAddress(), false));
            instructionHandler.sendInstruction(Instruction.DND);
            return;
        }

        server.log(new LogEntry(Instruction.GET, filename, socket.getRemoteSocketAddress(), true));
    }

    /**
     * Method to organize the Upload of a file to the server.
     *
     * @throws IOException if the server gets an unexpected response from the server during the communication
     */
    private void handlePUT() throws IOException {
        String filename = reader.readLine();
        instructionHandler.sendInstruction();

        String in = reader.readLine();

        if (!in.equals(Instruction.DAT.toString())) {
            server.log(new LogEntry(Instruction.PUT, filename, socket.getRemoteSocketAddress(), false));
            instructionHandler.sendInstruction(Instruction.DND);
            return;
        }

        int size = Integer.parseInt(reader.readLine());
        byte[] res = new byte[size];
        for (int i = 0; i < size; i++) {
            res[i] = (byte) reader.read();
        }
        boolean success = server.getFileOperator().writeFile(filename, res);

        if (success) instructionHandler.sendInstruction();
        else instructionHandler.sendInstruction(Instruction.DND);

        server.log(new LogEntry(Instruction.PUT, filename, socket.getRemoteSocketAddress(), success));
    }

    /**
     * Method to delete a file on the server.
     *
     * @throws IOException if the server gets an unexpected response from the server during the communication
     */
    private void handleDEL() throws IOException {
        String filename;
        boolean successful;
        filename = reader.readLine();
        successful = server.getFileOperator().deleteFile(filename);
        server.log(new LogEntry(Instruction.DEL, filename, socket.getRemoteSocketAddress(), successful));

        if (successful) instructionHandler.sendInstruction();
        else instructionHandler.sendInstruction(Instruction.DND);
    }

    /**
     * Method to list all files on the server.
     *
     * @throws IOException if the server gets an unexpected response from the server during the communication
     */
    private void handleLST() throws IOException {
        instructionHandler.sendInstruction();

        String in = reader.readLine();
        if (!in.equals(Instruction.ACK.toString())) {
            server.log(new LogEntry(Instruction.LST, "", socket.getRemoteSocketAddress(), false));
            return;
        }

        String[] files = server.getFileOperator().getFiles();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (String s : files) byteArrayOutputStream.write((s + "\r\n").getBytes());
        Instruction.DAT.send(writer, byteArrayOutputStream);
        byteArrayOutputStream.close();

        in = reader.readLine();
        if (!in.equals(Instruction.ACK.toString())) {
            server.log(new LogEntry(Instruction.LST, "", socket.getRemoteSocketAddress(), false));
            return;
        }

        server.log(new LogEntry(Instruction.LST, "", socket.getRemoteSocketAddress(), true));
    }

    /**
     * Method to terminate the connection.
     */
    private void handleDSC() {
        server.log(new LogEntry(Instruction.DSC, "", socket.getRemoteSocketAddress(), true));
        closeConnection();
    }
}
