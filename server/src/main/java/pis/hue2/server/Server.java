package pis.hue2.server;

import pis.hue2.common.FileOperator;
import pis.hue2.common.Instruction;
import pis.hue2.common.util.LogEntry;
import pis.hue2.common.util.SyncList;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The class server is responsible for start and stop of the server furthermore it checks the connection conditions.
 */
public class Server {

    /**
     * the port where the server will listen for incoming messages
     */
    private final int port;

    /**
     * thread-safe fileOperator
     */
    private final FileOperator fileOperator;

    /**
     * maximum number of connections between server and client
     */
    private static final int MAX_CONNECTION = 3;

    /**
     * Server socket to establish a connection to the client
     */
    private ServerSocket serverSocket;

    /**
     * synchronized list with logentries
     */
    private final SyncList<LogEntry> log = new SyncList<>();

    /**
     * synchronized list with the amount of sessions between the server and the client
     */
    private final SyncList<Session> sessions = new SyncList<>();

    /**
     * Constructor of the server.
     *
     * @param port         the port where the server will listen for incoming messages
     * @param fileOperator threadsafe filesystem accessor
     */
    public Server(int port, FileOperator fileOperator) {
        this.port = port;
        this.fileOperator = fileOperator;
    }

    /**
     * Method starts the server if the server socket isn't closed.
     * Starts the server on the port specified in the constructor.
     */
    public void startServer() {
        try {
            serverSocket = new ServerSocket(port);
            while (!serverSocket.isClosed()) {
                Socket s = serverSocket.accept();
                acceptConnection(s);
            }
        } catch (IOException e) {
            System.out.println("Server is closed");
        }
    }

    /**
     * Closes all sessions and closes the ServerSocket.
     */
    public void stopServer() {
        for (int i = 0; i < sessions.getSize(); i++) {
            sessions.get(0).closeConnection();
        }
        File f = new File("server.log");
        try (FileOutputStream fos = new FileOutputStream(f)) {
            if (serverSocket != null) serverSocket.close();
            for (int i = 0; i < log.getSize(); i++) {
                fos.write((log.get(i).toString() + "\n").getBytes());
            }
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Methods accepts connection to the client, if the maximum amount of connections is not reached yet.
     * If the connection is accepted, the methods adds a new session to the server and starts the session.
     * If the connection is refused the declined method is called.
     *
     * @param socket with which the server should connect
     * @throws IOException if the client send an unexpected instruction to the server
     */
    private void acceptConnection(Socket socket) throws IOException {
        boolean accept = true;
        if (sessions.getSize() >= MAX_CONNECTION) accept = false;
        else {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String message = bufferedReader.readLine();
                if (!message.equals(Instruction.CON.toString()))
                    accept = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (accept) {
            Session session = new Session(this, socket);
            sessions.add(session);
            new Thread(session).start();
        } else declineConnection(socket);

    }

    /**
     * Checks whether a session has been removed.
     *
     * @param session the Session that shall be removed
     */
    public void removeSession(Session session) {
        this.sessions.remove(session);
    }

    /**
     * Adds a LogEntry to the serverlog.
     *
     * @param l the LogEntry that shall be added to the serverlog
     */
    public void log(LogEntry l) {
        this.log.add(l);
    }

    /**
     * Gets the FileOperator.
     *
     * @return the threadSafe FileOperator of the Server
     */
    public FileOperator getFileOperator() {
        return this.fileOperator;
    }

    /**
     * Method declines the connection and sends as specified in the protocol an DND to the client
     *
     * @param socket communication socket of the server and the client, which has to be dismantled
     * @throws IOException if the socket can not be closed in the right way
     */
    private void declineConnection(Socket socket) throws IOException {
        PrintWriter pw = new PrintWriter(socket.getOutputStream());
        pw.println(Instruction.DND.toString());
        pw.flush();
        socket.close();
    }
}
