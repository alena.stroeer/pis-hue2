package pis.hue2.server;

import pis.hue2.common.FileOperator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Executable class to launch the server.
 */
public class LaunchServer {
    /**
     * Executable main method creates a server and starts a thread-safe session.
     *
     * @param args are not used
     */
    public static void main(String[] args) {
        FileOperator fileOperator = new FileOperator("../Files/");
        Server server = new Server(12222, fileOperator);
        new Thread(server::startServer).start();

        System.out.println("Server started on port '12222', files will be under 'Files/'.");

        BufferedReader waiter = new BufferedReader(new InputStreamReader(System.in));
        try {
            waiter.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.stopServer();
    }
}
