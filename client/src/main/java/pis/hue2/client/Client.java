package pis.hue2.client;

import pis.hue2.common.FileOperator;
import pis.hue2.common.Instruction;
import pis.hue2.common.InstructionHandler;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;


/**
 * The client class is responsible for  the interaction between server and client.
 */
public class Client {

    /**
     * Socket to establish a connection to the server
     */
    private static Socket socket;

    /**
     * BufferedReader to read the incoming instructions from the server
     */
    private BufferedReader reader;

    /**
     * PrintWriter to send instructions to the server
     */
    private PrintWriter writer;

    /**
     * InstructionHandler to get access to methods of the InstructionHandler
     */
    private InstructionHandler instructionHandler;

    /**
     * The method tries to establish a connection to the server.
     * If this is successful, it also creates the reader and writer for the communication with the server.
     * Furthermore it reports about the success or failure in the LogEntry.
     *
     * @param server to which the client should connect
     * @param port   which the client should use to establish the connection with the server
     * @throws IOException if the connection to the server could not be established
     */
    public void connect(String server, int port) throws IOException {
        socket = new Socket(server, port);
        writer = new PrintWriter(socket.getOutputStream());
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        instructionHandler = new InstructionHandler(reader, writer, socket.getRemoteSocketAddress());

        instructionHandler.sendInstruction(Instruction.CON, Instruction.ACK, "Es konnte keine Verbindung aufgebaut werden");
    }

    /**
     * The method tries to disconnect the connection to the server
     *
     * @throws IOException if the connection cannot be dismantled
     */
    public void disconnect() throws IOException {
        instructionHandler.sendInstruction(Instruction.DSC, Instruction.DSC, "Konnte die Verbindung zum Server nicht abbauen.");
        socket.close();
    }

    /**
     * Lists the files on the server.
     *
     * @return String-Array with contains the files/filenames of the server.
     * @throws IOException if the client gets an unexpected response from the server during the communication
     */
    public synchronized String[] list() throws IOException {
        instructionHandler.sendInstruction(Instruction.LST, Instruction.ACK, "Server akzeptiert die LST-Anfrage nicht.");
        instructionHandler.sendInstruction(Instruction.ACK, Instruction.DAT, "Server hat kein DAT zurück geschickt!");

        reader.readLine();
        ArrayList<String> result = new ArrayList<>();
        while (reader.ready()) {
            String line = reader.readLine();
            if (line.equals("")) {
                continue;
            }
            result.add(line);
        }

        instructionHandler.sendInstruction();
        return result.toArray(String[]::new);
    }

    /**
     * Uploads a file to the server.
     *
     * @param filename file that has to be uploaded
     * @throws IOException if the client gets an unexpected response from the server during the communication.
     */
    public synchronized void upload(File filename) throws IOException {
        instructionHandler.sendInstruction(Instruction.PUT, "Server akzeptiert die PUT-Anfrage nicht!", filename.getName());

        ByteArrayOutputStream data = new ByteArrayOutputStream();
        try (FileInputStream fis = new FileInputStream(filename)) {
            data.write(fis.readAllBytes());
        } catch (FileNotFoundException e) {
            throw new IOException("Die Datei wurde beim Client nicht gefunden!");
        }

        Instruction.DAT.send(writer, data);
        instructionHandler.receivePromise(Instruction.DAT, Instruction.ACK, "Nach erhalten der Daten, hat der Server kein ACK zurück geschickt!", "");
    }

    /**
     * Downloads a file from the server.
     *
     * @param path     were to download the file
     * @param filename of the file to be downloaded
     * @throws IOException if the client gets an unexpected response from the server during the communication
     */
    public synchronized void download(String path, String filename) throws IOException {
        instructionHandler.sendInstruction(Instruction.GET, "Server schickt kein ACK zurück!", filename);
        instructionHandler.sendInstruction(Instruction.ACK, Instruction.DAT, "Server schickt keine Daten bzw. die Daten falsch zum Client!");

        int size = Integer.parseInt(reader.readLine());
        byte[] data = new byte[size];
        for (int i = 0; i < size; i++) {
            data[i] = (byte) reader.read();
        }

        new FileOperator(path).writeFile(filename, data);

        instructionHandler.sendInstruction();
    }

    /**
     * Deletes a file from the server.
     *
     * @param filename of the file to be deleted
     * @throws IOException if the client gets an unexpected response from the server during the communication
     */
    public void delete(String filename) throws IOException {
        instructionHandler.sendInstruction(Instruction.DEL, "Löschanfrage wurde mit Denied abgelehnt!", filename);
    }
}


