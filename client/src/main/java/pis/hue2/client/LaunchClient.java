package pis.hue2.client;

/**
 * Executable class to launch the client.
 */
public class LaunchClient {
    /**
     * Executable main-method.
     *
     * @param args are not used
     */
    public static void main(String[] args) {
        new ClientGUI();
    }
}
