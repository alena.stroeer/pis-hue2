package pis.hue2.client;

import pis.hue2.common.util.Logger;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.IOException;

/**
 * ClientGUI implements the graphical user interface from the client's view.
 * It also implements the ActionPressed methods and calls the methods of the class Client.
 */
public class ClientGUI extends JFrame {

    /**
     * a variable from type Client, which allows the access to the methods from the class Client
     */
    private final Client client;

    /**
     * Textfield to which a server addresses can be passed or retrieved
     */
    private JTextField server;

    /**
     * Textfield to which a port can be passed or retrieved
     */
    private JTextField port;

    /**
     * Button to trigger the upload of files
     */
    private JButton uploadButton;

    /**
     * Button to trigger the download of files
     */
    private JButton downloadButton;

    /**
     * Button to trigger the deletion of files
     */
    private JButton deleteButton;

    /**
     * Button to trigger the connection/disconnection- establishment
     */
    private JButton connectionButton;

    /**
     * Button to trigger the listing of files
     */
    private JButton listButton;

    /**
     * Component to represent the folder structure and files of the client
     */
    private JFileChooser fileChooserClient;

    /**
     * List to represent the files on the server
     */
    private JList<String> filesServer;

    /**
     * Textarea to display the logentries
     */
    private JTextArea logTextArea;

    /**
     * Boolean to check the connection status
     */
    private boolean connected;

    /**
     * Constructor, creates the GUI and the general layout.
     * Adds the required PropertyChangeListener
     */
    public ClientGUI() {
        setTitle("Client");
        setSize(1475, 800);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        createGUI();
        setVisible(true);

        client = new Client();
        fileChooserClient.addPropertyChangeListener(filesClientSelected());
        filesServer.addListSelectionListener(filesServerSelected());

        Logger.getInstance().getPropertyChangeSupport().addPropertyChangeListener(logTextAreaListener());
        Logger.getInstance().getPropertyChangeSupport().addPropertyChangeListener(logConsoleListener());
    }

    /**
     * Adds the different sections of the BorderLayout to the GUI.
     */
    private void createGUI() {
        Container pane = getContentPane();
        BorderLayout layout = new BorderLayout(0, 0);
        pane.setLayout(layout);

        addTopPanel(pane);
        addLeftPanel(pane);
        addCenterPanel(pane);
        addRightPanel(pane);
        addBottomPanel(pane);
    }

    /**
     * Creates and adds the northern section of the BorderLayout to the GUI.
     * Northern section is responsible for the server and port input and the connect and disconnect button.
     *
     * @param pane Container must be handed over to add Components to the GUI
     */
    private void addTopPanel(Container pane) {
        JLabel serverLabel = new JLabel("Server:");
        server = new JTextField("localhost", 85);


        JLabel portLabel = new JLabel("Port:");
        port = new JTextField("12222", 30);

        connectionButton = new JButton("Connect");
        connectionButton.addActionListener(connectionButtonPressed());
        JButton exitButton = new JButton("Exit program");
        exitButton.addActionListener(exitButtonPressed());

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        panel.add(serverLabel);
        panel.add(server);
        panel.add(portLabel);
        panel.add(port);
        panel.add(connectionButton);
        panel.add(exitButton);

        pane.add(panel, BorderLayout.NORTH);
    }

    /**
     * Creates and adds the western section of the BorderLayout to the GUI.
     * Western Section is responsible for the folder structure and files of the client.
     *
     * @param pane Container must be handed over to add Components to the GUI
     */
    private void addLeftPanel(Container pane) {

        fileChooserClient = new JFileChooser();
        fileChooserClient.setControlButtonsAreShown(false);
        fileChooserClient.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooserClient.setMultiSelectionEnabled(false);
        pane.add(fileChooserClient, BorderLayout.WEST);
    }

    /**
     * Creates and adds the central section of the BorderLayout to the GUI.
     * Central section is responsible for the Buttons to interact between the client and the server.
     *
     * @param pane Container must be handed over to add the Components to the GUI
     */
    private void addCenterPanel(Container pane) {
        listButton = new JButton("<html><center>Datein<br />auflisten</center></html>");
        listButton.addActionListener(listButtonPressed());
        listButton.setEnabled(false);
        uploadButton = new JButton("<html><center>Upload<br />to<br />server</center></html>");
        uploadButton.addActionListener(uploadButtonPressed());
        uploadButton.setEnabled(false);
        downloadButton = new JButton("<html><center>Download<br />from<br />server</center></html>");
        downloadButton.addActionListener(downloadButtonPressed());
        downloadButton.setEnabled(false);
        deleteButton = new JButton("<html><center>Delete<br />from<br />server</center></html>");
        deleteButton.addActionListener(deleteButtonPressed());
        deleteButton.setEnabled(false);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(9, 0));
        addButtons(buttonPanel, listButton);
        addButtons(buttonPanel, uploadButton);
        addButtons(buttonPanel, downloadButton);
        addButtons(buttonPanel, deleteButton);
        pane.add(buttonPanel, BorderLayout.CENTER);
    }

      /**  Adds button to the given Panel
     * @param buttonPanel Panel has to be handed over to add the components to the buttonPanel
     * @param button button to be added
     */
    public void addButtons(JPanel buttonPanel, JButton button) {
        buttonPanel.add(new Box.Filler(new Dimension(), new Dimension(), new Dimension()));
        buttonPanel.add(button);
    }

    /**
     * Creats and adds the eastern section of the BorderLayout to the GUI.
     * Easrern section is responsible for the files of the server.
     *
     * @param pane Container must be handed over to add the Components to the GUI
     */
    private void addRightPanel(Container pane) {
        filesServer = new JList<>(new String[]{""});
        filesServer.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        filesServer.setLayoutOrientation(JList.VERTICAL);
        filesServer.setFixedCellWidth(675);

        JScrollPane scrollPane = new JScrollPane(
                filesServer,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
        );

        pane.add(scrollPane, BorderLayout.EAST);
    }

    /**
     * Creates the southern section of the BorderLayout to the GUI.
     * Southern section is responsible for the LogEntries.
     *
     * @param pane Container must be handed over to add the Components to the GUI
     */
    private void addBottomPanel(Container pane) {
        logTextArea = new JTextArea();
        logTextArea.setRows(10);
        logTextArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(
                logTextArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
        );

        scrollPane.getVerticalScrollBar().addAdjustmentListener(e -> e.getAdjustable().setValue(e.getAdjustable().getMaximum()));
        pane.add(scrollPane, BorderLayout.SOUTH);
    }

    /**
     * ActionEvent, which will be executed when the connection-Button is pressed.
     * Depending on the connection status, either the connect method or the disconnect method is called.
     * If the methods throw Exceptions the errors were added to the LogEntry of the GUI.
     *
     * @return ActionEvent, depending on the connection status
     */
    private ActionListener connectionButtonPressed() {
        return actionEvent -> {
            try {
                if (connected) {
                    disconnect();
                } else {
                    connect();
                }
            } catch (IOException e) {
                printExceptionInLogTextArea(e);
            }
        };
    }

    /**
     * Calls the client-connect method and hands over the parameters obtained from the input,furthermore enable the list
     * and upload button. Also calls the method client-list to display the files on the server in the GUI.
     *
     * @throws IOException if the client could not establish a connection to the server
     */
    private void connect() throws IOException {
        client.connect(server.getText(), Integer.parseInt(port.getText()));
        connected = true;
        listButton.setEnabled(true);
        connectionButton.setText("Disconnect");
        if (fileChooserClient.getSelectedFile() == null) {
            uploadButton.setEnabled(false);
        } else {
            uploadButton.setEnabled(fileChooserClient.getSelectedFile().isFile());
        }
        filesServer.setListData(client.list());
    }

    /**
     * Calls the client-disconnect method and deactivates the buttons that are needed to interact with the server
     *
     * @throws IOException if the client could not clear the connection to the server
     */
    private void disconnect() throws IOException {
        client.disconnect();
        connected = false;
        deleteButton.setEnabled(false);
        uploadButton.setEnabled(false);
        downloadButton.setEnabled(false);
        listButton.setEnabled(false);
        connectionButton.setText("Connect");
    }

    /**
     * Checks whether a file is selected in the FileChooser and enables the upload button as a consequence.
     *
     * @return PropertyChangelistener which pursues the FileChooser of the client
     */
    public PropertyChangeListener filesClientSelected() {
        return propertyChangeEvent -> {
            if (!connected || fileChooserClient == null || fileChooserClient.getSelectedFile() == null) {
                uploadButton.setEnabled(false);
            } else {
                uploadButton.setEnabled(fileChooserClient.getSelectedFile().isFile());
            }
        };
    }

    /**
     * Checks whether a file is selected in the List and enables the download and delete button as a consequence.
     *
     * @return ListSelectionListener which pursues the filelist of the server
     */
    public ListSelectionListener filesServerSelected() {
        return listSelectionEvent -> {
            if (!connected || filesServer.getComponentCount() <= 0 || filesServer.getSelectedValue() == null) {
                downloadButton.setEnabled(false);
                deleteButton.setEnabled(false);
            } else {
                boolean filesServerSelected = !filesServer.getSelectedValue().equals("\r\n");
                downloadButton.setEnabled(filesServerSelected);
                deleteButton.setEnabled(filesServerSelected);
            }
        };
    }

    /**
     * Creates a new LogEntry and expands the LogEntry in the LogTextArea.
     *
     * @return PropertyChangeListener which expands the LogEntry
     */
    private PropertyChangeListener logTextAreaListener() {
        return propertyChangeEvent -> logTextArea.append(propertyChangeEvent.getNewValue().toString() + "\r\n");
    }

    /**
     * Method outputs the LogEntrys in the Console.
     *
     * @return PropertyChangeListener which outputs the LogEntry in the console
     */
    private PropertyChangeListener logConsoleListener() {
        return propertyChangeEvent -> System.out.println(propertyChangeEvent.getNewValue().toString());
    }

    /**
     * Calls the client-upload method and hands over the parameters obtained from the input.
     * Furthermore the client-list method is called to display the added file on the server in the GUI.
     * If the methods throw Exceptions the errors were added to the LogEntry of the GUI.
     *
     * @return ActionListener for the upload button
     */
    private ActionListener uploadButtonPressed() {
        return actionEvent -> new Thread(() -> {
            try {
                client.upload(fileChooserClient.getSelectedFile());
                filesServer.setListData(client.list());
            } catch (IOException e) {
                printExceptionInLogTextArea(e);
            }
        }).start();
    }

    /**
     * Calls the client-list method and displays the data in the Textarea of the serverfiles.
     * If the methods throw Exceptions the errors were added to the LogEntry of the GUI.
     *
     * @return ActionListener for the list button
     */
    private ActionListener listButtonPressed() {
        return actionEvent -> new Thread(() -> {
            try {
                filesServer.setListData(client.list());
            } catch (IOException e) {
                printExceptionInLogTextArea(e);
            }
        }).start();
    }

    /**
     * Calls the client-download method and hands over the parameters obtained from the input.
     * Furthermore it updates the GUI on the client section to display the changed situation after the download.
     * If the methods throw Exceptions the errors were added to the LogEntry of the GUI.
     *
     * @return Actionlistener for the download button
     */
    private ActionListener downloadButtonPressed() {
        return actionEvent -> new Thread(() -> {
            try {
                client.download(
                        fileChooserClient.getCurrentDirectory().getAbsolutePath(),
                        filesServer.getSelectedValue().replace("\r\n", "")
                );
                fileChooserClient.updateUI();
            } catch (IOException e) {
                printExceptionInLogTextArea(e);
            }
        }).start();
    }

    /**
     * Calls the client-delete method and hands over the parameters obtained from the input.
     * Furthermore the client-list method is called to display the deleted file on the server in the GUI.
     * If the methods throw Exceptions the errors were added to the LogEntry of the GUI.
     *
     * @return ActionListener for the delete button
     */
    private ActionListener deleteButtonPressed() {
        return actionEvent -> new Thread(() -> {
            try {
                client.delete(filesServer.getSelectedValue());
                filesServer.setListData(client.list());
            } catch (IOException e) {
                printExceptionInLogTextArea(e);
            }
        }).start();
    }

    /**
     * Prints an exception message in the LogTextArea
     *
     * @param e Exception which has to be printed in the Log
     */
    private void printExceptionInLogTextArea(IOException e) {
        e.printStackTrace();
        logTextArea.append("Got IOException: " + "\r\n");
        logTextArea.append(e.getMessage() + "\r\n");

    }

    /**
     * Tries to call the client-disconnect method and sends the exit code to stop the program.
     *
     * @return ActionListener for the exit button
     */
    private ActionListener exitButtonPressed() {
        return actionEvent -> {
            try {
                if (connected) {
                    client.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.exit(DO_NOTHING_ON_CLOSE);
        };
    }
}


