package pis.hue2.common.util;

import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

/**
 * The class is responsible for the constant existence and expansion of the LogFile
 */
public class Logger {
    private static Logger instance;

    /**
     * List to creat LogEntries for the GUI
     */
    private final List<LogEntry> logs;

    /**
     * PropertyChangeSupport gets fired on logging events
     */
    private final PropertyChangeSupport propertyChangeSupport;

    /**
     * Constructor of the class Logger.
     */
    private Logger() {
        logs = new ArrayList<>();
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    /**
     * Ensures that the Logger exists and return the Logger.
     *
     * @return instance of the Logger
     */
    public static Logger getInstance() {
        if (Logger.instance == null) {
            Logger.instance = new Logger();
        }
        return Logger.instance;
    }

    /**
     * Adds the new LogEntry to the Log to make sure that the log is continuously displayed
     *
     * @param logEntry which displays the complete actions carried out
     */
    public void addLogEntry(LogEntry logEntry) {
        propertyChangeSupport.firePropertyChange("addLog", logs, logEntry);
        logs.add(logEntry);
    }

    /**
     * Gets the PropertyChangeSupport
     *
     * @return PropertyChangeSupport
     */
    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }
}
