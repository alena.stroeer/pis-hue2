package pis.hue2.common.util;

import java.util.ArrayList;
import java.util.List;

/**
 * A threadSafe List wrapper.
 *
 * @param <T> item Type
 */
public class SyncList<T> {
    private final List<T> list = new ArrayList<>();

    /**
     * Gets the listsize.
     *
     * @return the size of the list
     */
    public synchronized int getSize() {
        return list.size();
    }

    /**
     * Adds an element to the SyncList.
     *
     * @param element adds the element to the List
     */
    public synchronized void add(T element) {
        list.add(element);
    }

    /**
     * Gets the element with the index i.
     *
     * @param i is the index that shall be returned
     * @return the element with the index i
     * @throws IndexOutOfBoundsException if the index is out of  range
     */
    public synchronized T get(int i) {
        return list.get(i);
    }

    /**
     * Removes an element from the SyncList.
     *
     * @param element that shall be removed
     */
    public synchronized void remove(T element) {
        list.remove(element);
    }
}
