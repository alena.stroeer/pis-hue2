package pis.hue2.common.util;

import pis.hue2.common.Instruction;

import java.net.SocketAddress;
import java.sql.Timestamp;

/**
 * Class to create and format the LogEntry.
 */
public class LogEntry {

    /**
     * Instruction to be send
     */
    private final Instruction send;

    /**
     * filename to be send and processed
     */
    private final String filename;

    /**
     * boolean whether the instruction was correctly received or processed
     */
    private final boolean successful;

    /**
     * address of the communication socket
     */
    private final SocketAddress address;

    /**
     * format of the timestamp
     */
    private final Timestamp timestamp;

    /**
     * the instruction received from the communication partner
     */
    private final Instruction response;

    /**
     * LogEntry with an sent instruction.
     *
     * @param instruction that shall be logged
     * @param filename    the file that was accessed
     * @param address     the remote address
     * @param successful  if the execution of the instruction was successful
     */
    public LogEntry(Instruction instruction, String filename, SocketAddress address, boolean successful) {
        this.address = address;
        this.filename = filename;
        this.send = instruction;
        this.successful = successful;
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.response = null;
    }

    /**
     * LogEntry with a sent and receive message .
     *
     * @param send       instruction that shall be logged
     * @param filename   the file that was accessed
     * @param address    the remote address
     * @param successful if the execution of the sent instruction was successfull
     * @param response   instruction that shall be received and logged
     */
    public LogEntry(Instruction send,
                    String filename,
                    SocketAddress address,
                    boolean successful,
                    Instruction response) {
        this.address = address;
        this.filename = filename;
        this.send = send;
        this.successful = successful;
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.response = response;
    }

    /**
     * Method to differentiate between the individual string formats and to generate an easily readable LogEntry.
     *
     * @return a better readable string of the LogEntry
     */
    @Override
    public String toString() {
        String format = "%s, %s, Gesendet: %s, %s";
        format += (!filename.equals("")) ? ", Datei: %s" : "%s";
        format += (response != null) ? ", Empfangen: %s" : "%s";

        return String.format(
                format,
                timestamp.toString(),
                address.toString(),
                send.toString(),
                (successful ? "Success" : "Fail"),
                (!filename.equals("")) ? filename : "",
                (response != null) ? response.toString() : ""
        );
    }
}
