package pis.hue2.common;

import pis.hue2.common.util.LogEntry;
import pis.hue2.common.util.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketAddress;

/**
 * Handles the sending and receiving of instructions.
 */
public class InstructionHandler {

    /**
     * BufferedReader to read the incoming instructions from the communication partner
     */
    private final BufferedReader reader;

    /**
     * PrintWriter to send instructions to the communication partner
     */
    private final PrintWriter writer;

    /**
     * Socket address via which the interlocutors communicate
     */
    private final SocketAddress remoteAddress;

    /**
     * Constructor of the class InstructionHandler
     *
     * @param reader        to read the incoming instructions
     * @param writer        to send instructions
     * @param remoteAddress communication socket address
     */
    public InstructionHandler(BufferedReader reader, PrintWriter writer, SocketAddress remoteAddress) {
        this.reader = reader;
        this.writer = writer;
        this.remoteAddress = remoteAddress;
    }

    /**
     * Send the instruction ACK to the communication partner without expecting an answer from him
     */
    public void sendInstruction() {
        Instruction.ACK.send(writer);
        Logger.getInstance().addLogEntry(new LogEntry(Instruction.ACK, "", remoteAddress, true));
    }

    /**
     * Sends a single instruction to the communication partner without expecting an answer
     * @param sendInstruction Instruction to be send to the communication partner
     */
    public void sendInstruction(Instruction sendInstruction) {
        sendInstruction.send(writer);
        Logger.getInstance().addLogEntry(new LogEntry(sendInstruction, "", remoteAddress, true));
    }

    /**
     * Send an instruction and expect a special instruction as response.
     *
     * @param sendInstruction Instruction to be send to the communication partner
     * @param promise         Instruction to be send back from the communication partner if successful
     * @param errorMessage    to be output if the communication was not successful
     * @throws IOException if the who sent something receives an unexpected message
     */
    public void sendInstruction(Instruction sendInstruction,
                                Instruction promise,
                                String errorMessage)
            throws IOException {
        sendInstruction.send(writer);
        receivePromise(sendInstruction, promise, errorMessage, "");
    }

    /**
     * Send an instruction and a filename.
     *
     * @param sendInstruction Instruction to be send to the communication partner
     * @param errorMessage    to be output if the communication was not successful
     * @param filename        of the file to be edited
     * @throws IOException if the who sent something receives an unexpected message
     */
    public void sendInstruction(Instruction sendInstruction, String errorMessage, String filename) throws IOException {
        sendInstruction.send(writer, filename);
        receivePromise(sendInstruction, Instruction.ACK, errorMessage, filename);
    }

    /**
     * Send an instruction and a filename and expect a special instruction as response.
     *
     * @param sendInstruction Instruction to be send to the communication partner
     * @param promise         Instruction to be send back from server if successful
     * @param errorMessage    to be output if the communication was not successful
     * @param filename        of the file to be edited
     * @throws IOException if the who sent something receives an unexpected message
     */
    public void receivePromise(Instruction sendInstruction, Instruction promise, String errorMessage, String filename) throws IOException {
        Instruction response = Instruction.receive(reader);
        if (response != promise) {
            Logger.getInstance().addLogEntry(new LogEntry(sendInstruction, filename, remoteAddress, false, response));
            throw new IOException(errorMessage);
        }
        Logger.getInstance().addLogEntry(new LogEntry(sendInstruction, filename, remoteAddress, true, response));
    }
}
