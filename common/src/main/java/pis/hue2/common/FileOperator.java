package pis.hue2.common;

import java.io.*;

/**
 * threadSafe FileOperator
 */
public class FileOperator {
    private final String path;

    /**
     * Constructor of the class FileOperator.
     *
     * @param path to the root directory
     */
    public FileOperator(String path) {
        this.path = path;
    }

    /**
     * Gets a Array of files.
     *
     * @return a String[] of the files in the directory defined in constructor
     */
    public synchronized String[] getFiles() {
        return new File(path).list((file, name) -> !name.startsWith("."));
    }

    /**
     * Gets a ByteArrayOutputStream of files.
     *
     * @param filename of the file that shall be accessed
     * @return a ByteArrayOutputStream of the byteData in the file, null if the file was not found.
     */
    public synchronized ByteArrayOutputStream getFile(String filename) {
        File f = new File(path + "/" + filename);
        ByteArrayOutputStream fileBuffer = new ByteArrayOutputStream();
        try (FileInputStream fis = new FileInputStream(f)) {
            fileBuffer.write(fis.readAllBytes());
            fileBuffer.flush();
            return fileBuffer;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Delets the given file.
     *
     * @param filename of the file that shall be deleted
     * @return weather the file could be deleted successful
     */
    public synchronized boolean deleteFile(String filename) {
        return new File(path + "/" + filename).delete();
    }

    /**
     * Writes a file.
     *
     * @param filename of the file that shall be created and filled with data
     * @param data     the data that shall be written to the file
     * @return weather the file operation was successful
     */
    public synchronized boolean writeFile(String filename, byte[] data) {
        File f = new File(path + "/" + filename);
        try (FileOutputStream fos = new FileOutputStream(f)) {
            fos.write(data);
            fos.flush();
            return true;

        } catch (IOException e) {
            return false;
        }
    }
}
