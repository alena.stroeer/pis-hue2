package pis.hue2.common;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * The enum provides the instruction via which server and client communicate.
 * Furthermore it contains the method to send and receive this instructions.
 */
public enum Instruction {
    CON, DSC, ACK, DND, LST, PUT, GET, DEL, DAT;

    /**
     * The method sends the instruction DAT and the related information.
     * sends data like defined in protocol (DAT size data)
     *
     * @param writer the PrintWriter where the data should be send to
     * @param data   the data ByteArrayOutputStream that should be send
     */
    public void send(PrintWriter writer, ByteArrayOutputStream data) {
        writer.println(Instruction.DAT.toString());
        writer.println(data.size());
        writer.print(data);
        writer.flush();
    }

    /**
     * The method sends the instruction to the communication partner and hands over the filename.
     *
     * @param writer    the PrintWriter where the data should be send to
     * @param filenames to be used for actions
     */
    public void send(PrintWriter writer, String... filenames) {
        writer.println(this.toString());
        for (String filename : filenames) {
            writer.println(filename);
        }
        writer.flush();
    }

    /**
     * The method processes the receiving of instructions.
     *
     * @param reader the BufferedReader who reads the incoming instructions
     * @return the value of the instruction read
     * @throws IOException if the communication partner sends an unexpected instruction as answer
     */
    public static Instruction receive(BufferedReader reader) throws IOException {
        return Instruction.valueOf(reader.readLine());
    }
}
